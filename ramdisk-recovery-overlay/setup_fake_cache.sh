#!/sbin/sh

DATA_MOUNT_CODE=1
mkdir -p /userdata

while [ "$DATA_MOUNT_CODE" != "0" ]; do
    mount -t ext4 /dev/block/bootdevice/by-name/linux /userdata > /dev/kmsg
    DATA_MOUNT_CODE=$?
    sleep 1
done

mkdir /userdata/cache > /dev/kmsg
mount -o bind /userdata/cache /cache > /dev/kmsg

BOOT_PARTITION=$(grep -o 'androidboot\.bootpartition=[A-z_-]*' /proc/cmdline | head -1 | cut -d "=" -f2)
if [ ! -z "$BOOT_PARTITION" ]; then
    echo "Active boot slot: $BOOT_PARTITION" > /dev/kmsg
    sed -i "s@by-name/boot @by-name/$BOOT_PARTITION @g" /etc/recovery.fstab
    echo "/dev/block/platform/bootdevice/by-name/$BOOT_PARTITION /boot boot defaults 0 0" >> /etc/fstab
    if [ "$BOOT_PARTITION" != "boot" ]; then
        for SYMLINK_PATH in /dev/block/bootdevice/by-name /dev/block/platform/bootdevice/by-name; do
            cd $SYMLINK_PATH
            mv boot boot.real || true
            ln -s $BOOT_PARTITION boot > /dev/kmsg
        done
    fi
fi

setprop halium.datamount.done 1

exit 0
